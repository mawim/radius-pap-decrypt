Tool to decrypt Radius PAP requests
===================================

If you have a package capture from the Radius connection, you most likely will
use whireshark to decrypt the password. In had the case, that I only had the
dump of a Radius request and needed to see whether the correct password is sent
by the client.

This small programm allows do decrypt the request under the following condition:
**the password must not be longer than 8 bytes.**

To use the two, three parameters have to be provided:

* the authenticator (parameter `-a`)
* the (encrypted) password (parameter `-p`)
* the shared secret (parameter `-s`)

The first two can be found in the Radius request as `Authenticator:` and as
`User-Password Attribute (2)`. The authenticator can be given as is, in the
password the spaces have to be removed.

```
192.168.1.144.57243 > 192.168.1.144.1812: [bad udp cksum 0x84d7 -> 0x44fd!] RADIUS, length: 77
    Access-Request (1), id: 0x2c, Authenticator: 106b49030bec802e1bd156ff48e3589a
      User-Name Attribute (1), length: 9, Value: testing
        0x0000: 7465 7374 696e 67
      User-Password Attribute (2), length: 18, Value:
        0x0000: d112 7b01 9c70 9548 d4a1 db33 c648 739e
      NAS-IP-Address Attribute (4), length: 6, Value: 127.0.1.1
        0x0000: 7f00 0101
      NAS-Port Attribute (5), length: 6, Value: 0
        0x0000: 0000 0000
      Message-Authenticator Attribute (80), length: 18, Value: &7u...s..L....|..
        0x0000: 2637 75c2 ead6 73cc d64c ee9f a97c d8d7
```

The shared secret has to be known. It's what has been configured in the Radius
server and the NAS.
