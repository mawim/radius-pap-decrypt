use hex::FromHexError;
use quick_error::*;

quick_error! {
    #[derive(Debug)]
    pub enum AppError {
        HexError(err: FromHexError) {
            description("error decoding hex input value")
                display("Error decoding a hex input string")
                cause(err)
                from()
        }
    }
}
