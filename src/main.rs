use clap::{clap_app, crate_version};
use err::AppError;
use hex::decode;

mod err;

fn main() -> Result<(), AppError> {
    let args = clap_app!(
        myapp =>
            (name: "radius-pap-decrypt")
            (version: crate_version!())
            (author: "Matthias Wimmer <m@tthias.eu>")
            (about: "Decodes the password in Radius PAP.")
            (@arg SECRET: -s --secret +takes_value +required
             "shared secret (between client and server)")
            (@arg AUTHENTICATOR: -a --authenticator +takes_value +required
             "Hex string of the authenticator value from the request")
            (@arg PASSWORD: -p --password +takes_value +required
             "Hex string of the encrypted password"))
    .get_matches();

    let authenticator = decode(args.value_of("AUTHENTICATOR").unwrap_or(""))?;
    let password = decode(args.value_of("PASSWORD").unwrap_or(""))?;
    let secret = args
        .value_of("SECRET")
        .unwrap_or("")
        .to_owned()
        .into_bytes();

    let secret_authenticator = [secret, authenticator].concat();
    let mask = md5::compute(&secret_authenticator[..]);
    let digest_bytes: [u8; 16] = mask.into();

    for i in 0..8 {
        if password.len() > i {
            let clear = digest_bytes[i] ^ password[i];
            let passchar = if clear >= 0x20 && clear < 0x7f {
                clear as char
            } else {
                ' '
            };
            println!(
                "Byte {}: Mask {:02x}, Password {:02x}, Result {:02x} {}",
                i,
                digest_bytes[i],
                password[i],
                digest_bytes[i] ^ password[i],
                passchar
            );
        }
    }

    Ok(())
}
